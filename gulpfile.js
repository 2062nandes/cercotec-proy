'use strict';
  const gulp = require('gulp'),
        postCss = require('gulp-postcss'),
        simpleVars = require('postcss-simple-vars'),
        nested = require('postcss-nested'),
        colorRgbaFallback = require('postcss-color-rgba-fallback'),
        opacity = require('postcss-opacity'),
        pseudoelements = require('postcss-pseudoelements'),
        vmin = require('postcss-vmin'),
        pixrem = require('pixrem'),
        willChange = require('postcss-will-change'),
        customMedia = require('postcss-custom-media'),
        mediaMinMax = require('postcss-media-minmax'),
        cssnano = require('cssnano'),
        pxtorem = require('postcss-pxtorem');// Convertir valores de px a rem basado en baseFontSize
  let postCssPlugins = [
    simpleVars,
    nested,
    cssnano({
      autoprefixer: {
        add:true
      },
      core: false
    }),
    colorRgbaFallback,
    opacity,
    pseudoelements,
    vmin,
    pixrem,
    willChange,
    customMedia,
    mediaMinMax,
    pxtorem
  ];
  gulp.task('styles', ()=>
    gulp.src('./src/*.css')
        .pipe(postCss(postCssPlugins))
        .pipe(gulp.dest('./css'))
  );
  gulp.task('default',()=>
    gulp.watch('./src/*.css',['styles'])
  );
