<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
  <div class="row">
    <div class="col s12">
  <h4>Parantes galvanizados</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 2 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes1.jpg" alt="parantes galvanizados">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 2 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes1.jpg" alt="parantes galvanizados">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 2 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes1.jpg" alt="parantes galvanizados">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
  </article>
</div></div>
  <div class="row">
    <div class="col s12">
  <h4>Postes</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 6 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes3.jpg" alt="postes - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 6 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes3.jpg" alt="postes - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 6 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes3.jpg" alt="postes - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
  </article>
</div></div>
  <div class="row">
    <div class="col s12">
  <h4>Mástiles</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. Diferentes alturas</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes3.jpg" alt="mástiles - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. Diferentes alturas</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes3.jpg" alt="mástiles - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. Diferentes alturas</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes3.jpg" alt="mástiles - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
  </article>
</div></div>
  <div class="row">
    <div class="col s12">
  <h4>Luminarias</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 8 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes2.jpg" alt="luminarias - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 8 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes2.jpg" alt="luminarias - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Cañerías galvanizadas. 8 m de altura</figcaption>
        <img class="materialboxed" data-original="images/parantes/parantes2.jpg" alt="luminarias - parantes">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
  </article>
</div></div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
