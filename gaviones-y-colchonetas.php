<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
  <div class="row">
    <div class="col s12">
      <h4>GAVIONES Y COLCHONETAS</h4>
      <article class="article-flex">
        <div class="col s12 m6 l7 right">
          <h5 class="center-align"><b>Solicite su cotización</b></h5><br>
              <form class="col s12" method="post" id="theForm2" class="second" action="cotizacion.php" role="form">
                        <div class="row margen-bottom">
                          <div class="input input-field col s12">
                            <i class="material-icons prefix">account_circle</i>
                            <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                            <label for="nombre">Nombre completo:</label>
                          </div>
                        </div>
                        <div class="row margen-bottom">
                          <div class="input input-field col s12">
                            <i class="material-icons prefix">settings_cell</i>
                            <input type="number" id="movil" class="validate" name="movil" tabindex="3" required>
                            <label for="movil">Teléfono móvil:</label>
                          </div>
                        </div>
                        <div class="row margen-bottom">
                          <div class="input input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <input type="email" id="email" class="validate" name="email" tabindex="6" required>
                            <label for="email">Su E-mail:</label>
                          </div>
                        </div>
                        <div class="row margen-bottom mensaje">
                          <div class="input input-field col s12">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea id="cotizacion" class="materialize-textarea validate" cols="55" rows="7" name="cotizacion" tabindex="7" required></textarea>
                            <label for="cotizacion">Detalles de su cotización:</label>
                          </div>
                        </div>
                        <div class="row margen-bottom botones center-align">
                          <!-- <i style="background-color: #0d47a1;" class="btn z-depth-3 waves-input-wrapper"><input class="waves-button-input" type="submit" tabindex="8" value="Enviar"></i> -->
                          <input style="background-color: #0d47a1;" class="submitbtn2 waves-effect waves-red btn" type="submit" tabindex="8" value="Enviar"> </input>
                          <input style="background-color: #0d47a1;" class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input>
                        </div>
                      <div class="col s12">
                        <div id="statusMessage"></div>
                      </div>
              </form>
        </div>
        <div class="col s12 m6 l5">
          <figure>
            <figcaption class="center-align"></figcaption>
            <img class="materialboxed" data-original="images/gaviones/gaviones-y-colchonetas.jpg" alt="Gaviones y Colchonetas - Cercotec">
            <figcaption class="right-align"></figcaption>
          </figure>
        </div>
      </article>
    </div>
  </div>
<div class="row">
 <div class="col s12">
  <h4>Gaviones</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Pieza: 2 x 1 x 1 m (sin diafragma)</figcaption>
        <img class="materialboxed" data-original="images/gaviones/gavion0.jpg" alt="gaviones -cercotec">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Pieza: 2 x 1 x 1 m (sin diafragma)</figcaption>
        <img class="materialboxed" data-original="images/gaviones/gavion1.jpg" alt="gaviones -cercotec">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Pieza: 2 x 1 x 1 m (sin diafragma)</figcaption>
        <img class="materialboxed" data-original="images/gaviones/gavion1.jpg" alt="gaviones -cercotec">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
  </article>
</div>
</div>
<div class="row">
 <div class="col s12">
  <h4>Colchonetas</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Pieza: 4 x 2 x 0,30 m</figcaption>
        <img class="materialboxed" data-original="images/gaviones/gavion2.jpg" alt="colchonetas - cercotec">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Pieza: 4 x 2 x 0,30 m</figcaption>
        <img class="materialboxed" data-original="images/gaviones/gavion2.jpg" alt="colchonetas - cercotec">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Pieza: 4 x 2 x 0,30 m</figcaption>
        <img class="materialboxed" data-original="images/gaviones/gavion2.jpg" alt="colchonetas - cercotec">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
  </article>
</div>
</div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
