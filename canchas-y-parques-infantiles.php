<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
<div class="row">
  <div class="col s12">
  <h4>CANCHAS Y PARQUES INFANTILES</h4>
  <article>
    <div class="col s12 m12 offset-l1 l10 construccion">
      <div class="slider">
        <ul class="slides">
          <li>
            <img src="images/construccion/construccion3.jpg">
            <div class="caption center-align">
              <h3>FABRICACION DE JUEGOS PARA PARQUES</h3>
              <h5 class="light grey-text text-lighten-3">Sube bajas, resbalines, etc</h5>
            </div>
          </li>
          <li>
            <img src="images/construccion/construccion5.jpg">
            <div class="caption right-align">
              <h3>ENMALLADO DE PARQUES INFANTILES</h3>
              <h5 class="light grey-text text-lighten-3">Refugios, bordillos, cercas altas, bajas</h5>
            </div>
          </li>
          <li>
            <img src="images/construccion/construccion7.jpg">
            <div class="caption left-align">
              <h3>ENMALLADO DE CAMPOS DEPORTIVOS</h3>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </article>
  </div>
</div>
  </div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
