<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
<div class="row">
  <div class="col s12">
  <h4>Alambre de púas</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3 x 3</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla10.jpg" alt="alambre de púas">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3 x 3</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla11.jpg" alt="alambre de púas">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Instalación</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla16.jpg" alt="alambre de púas">
        <figcaption class="right-align"></figcaption>
      </figure>
    </div>
  </article>
  </div>
</div>
  </div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
