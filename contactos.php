<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
<div class="row" style="margin-bottom:0;">
  <div class="col s12">
    <h4>Contáctenos</h4>
    <article>
      <p>ESCRÍBANOS! Recibimos pedidos desde cualquier punto del país. Simplemente llene este formulario indicándonos lo que necesita e inmediatamente tendremos el gusto de hacerle llegar una cotización para que realice un depósito en nuestra cuenta del Banco Unión Nº 117599678 en moneda nacional (Bs). Le garantizamos la entrega en cualquier ciudad o población en un transporte de carga que coordinaremos mutuamente y que le hará llegar su mercadería en la puerta de su negocio o domicilio en el tiempo que nos comprometamos.</p>
    </article>
  </div>
</div>
<div class="row">
  <div class="col s12 m5 l4">
  <h4>Escríbanos!</h4>
  <article>
      <form style="padding: 21px 4% 20px 1%;" method="post" id="theForm" class="second" action="mail.php" role="form">
                <div class="form_row">
                  <div class="input input-field">
                    <i class="material-icons prefix">account_circle</i>
                    <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                    <label for="nombre">Nombre completo:</label>
                  </div>
                </div>
                <div class="form_row">
                  <div class="input input-field">
                    <i class="material-icons prefix">phone</i>
                    <input type="number" id="telefono" class="validate" name="telefono" tabindex="2" required>
                    <label for="telefono">Teléfono:</label>
                  </div>
                </div>
                <div class="form_row">
                  <div class="input input-field">
                    <i class="material-icons prefix">settings_cell</i>
                    <input type="number" id="movil" class="validate" name="movil" tabindex="3" required>
                    <label for="movil">Teléfono móvil:</label>
                  </div>
                </div>
                <div class="form_row">
                  <div class="input input-field">
                    <i class="material-icons prefix">location_on</i>
                    <input type="text" id="direccion" class="validate" name="direccion" tabindex="4" required>
                    <label for="direccion">Dirección:</label>
                  </div>
                </div>
                <div class="form_row">
                  <div class="input input-field">
                    <i class="material-icons prefix">location_city</i>
                    <input type="text" id="ciudad" class="validate" name="ciudad" tabindex="5" required>
                    <label for="ciudad">Ciudad:</label>
                  </div>
                </div>
                  <div class="form_row">
                    <div class="input input-field">
                      <i class="material-icons prefix">email</i>
                      <label for="email">Su E-mail:</label>
                      <input type="email" id="email" class="validate" name="email" tabindex="6" required>
                    </div>
                  </div>

                  <div class="form_row mensaje">
                    <div class="input input-field">
                      <i class="material-icons prefix">mode_edit</i>
                      <label for="mensaje">Mensaje:</label>
                      <textarea id="mensaje" class="materialize-textarea validate" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                    </div>
                  </div>

                  <div class="form_row botones center-align">
                    <input style="background-color: #0d47a1;" class="submitbtn waves-effect waves-red btn" type="submit" tabindex="8" value="Enviar"> </input>
                    <input style="background-color: #0d47a1;" class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input>
                  </div>
              <div class="col s12">
                <div id="statusMessage"></div>
              </div>
      </form>
  </article>
  </div>
  <div class="col s12 m7 l8">
  <h4>Ubíquenos</h4>
  <article>
    <h5 class="center-align">La Paz - Oficina Central</h5>
      <iframe src="https://www.google.com/maps/d/embed?mid=1e7H-5_4QUXvmN6GhKSF-IWVDtEej4daJ" width="100%" height="480"></iframe>
    <h5 class="center-align">La Paz - Sucursal</h5>
      <iframe src="https://www.google.com/maps/d/embed?mid=14KCTxNy9Jj-HU_nZMMqdD6mo1p0" width="100%" height="480"></iframe>
    <h5 class="center-align">Oficina Tarija</h5>
      <iframe src="https://www.google.com/maps/d/embed?mid=1U-g3zRZUisyFj-fRrY-3TCSy2rblrBQy" width="100%" height="480"></iframe>
  </article>
  </div>
</div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
