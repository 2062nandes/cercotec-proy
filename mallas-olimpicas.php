<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
<div class="row">
  <div class="col s12">
  <h4>Malla Agropecuaria</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">10x10</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas agropecuarias - cercotec">
        <figcaption class="right-align">Alambre #10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">10x10</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla2.jpg" alt="mallas agropecuarias - cercotec">
        <figcaption class="right-align">Alambre #12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">10x10</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla3.jpg" alt="mallas agropecuarias - cercotec">
        <figcaption class="right-align">Alambre #14</figcaption>
      </figure>
    </div>
  </article>
</div>
</div>
<div class="row">
  <div class="col s12">
  <h4>Malla Olímpica</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">9x9</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">9x9</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla4.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">9x9</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla5.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">8x8</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla6.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">8x8</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla7.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">8x8</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla8.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">7x7</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla9.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">7x7</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">7x7</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla2.jpg" alt="mallas olímpicas">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
  </article>
  </div>
</div>
<div class="row">
  <div class="col s12">
  <h4>Malla Jardinera</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">6½ x 6½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas jardineras">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">6½ x 6½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas jardineras">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">6½ x 6½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas jardineras">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">6 x 6</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas jardineras">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">6 x 6</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas jardineras">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">6 x 6</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="mallas jardineras">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
  </article>
  </div>
</div>
<div class="row">
  <div class="col s12">
  <h4>Malla Industrial</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">5 x 5</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla industrial">
        <figcaption class="right-align">Alambre # 10</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">5 x 5</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla industrial">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">5 x 5</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla industrial">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">4 x 4</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla industrial">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">4 x 4</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla industrial">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Instalación</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla12.jpg" alt="malla industrial">
        <figcaption class="right-align">...</figcaption>
      </figure>
    </div>
  </article>
  </div>
</div>
<div class="row">
  <div class="col s12">
  <h4>Malla Arquitectónica</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3½ x 3½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla arquitectónica">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3½ x 3½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla arquitectónica">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Instalación</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla13.jpg" alt="malla arquitectónica">
        <figcaption class="right-align">...</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3 x 3</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla arquitectónica">
        <figcaption class="right-align">Alambre # 12</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3 x 3</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla arquitectónica">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Instalación</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla14.jpg" alt="malla arquitectónica">
        <figcaption class="right-align">...</figcaption>
      </figure>
    </div>
  </article>
  </div>
</div>
<div class="row">
  <div class="col s12">
  <h4>Malla Milimétrica</h4>
  <article>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3½ x 3½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla milimétrica">
        <figcaption class="right-align">Alambre # 14</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">3½ x 3½</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla1.jpg" alt="malla milimétrica">
        <figcaption class="right-align">Alambre # 16</figcaption>
      </figure>
    </div>
    <div class="col s12 m6 l4">
      <figure>
        <figcaption class="center-align">Instalación</figcaption>
        <img class="materialboxed" data-original="images/mallas/malla5.jpg" alt="malla milimétrica">
        <figcaption class="right-align">...</figcaption>
      </figure>
    </div>
  </article>
  </div>
</div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
