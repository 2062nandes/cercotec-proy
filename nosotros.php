<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
<div class="row" style="margin-bottom:0;">
  <div class="col s12 m6 l7 right">
  <h4>Nuestra empresa</h4>
  <article>
  <p>MALLAS OLÍMPICAS CERCOTEC inicia sus actividades el 6 de diciembre del año 2000. Actualmente la empresa presta servicios en todas las ciudades del país, especialmente, Oruro, Cochabamba, Potosí, Sucre, Tarija con la capacidad de expandir su mercado con tecnología nueva japonesa que le posibilitará llegar a ser empresa líder en el rubro.</p>
  </article>
  </div>
  <div class="col s12 m6 l5 left">
    <h4>Video Presentación</h4>
      <iframe width="100%" height="315" src="https://www.youtube.com/embed/tRs_NCCCSkU?rel=0&loop=1&autoplay=1" frameborder="0" allowfullscreen></iframe>
  </div>
</div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
