<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<div class="container">
  <!-- Start WOWSlider.com BODY section -->
	<div id="wowslider-container1">
	<div class="ws_images"><ul>
    <li><img src="data1/images/slider1.jpg" srcset="data1/images/slider1.jpg 1280w, data1/images/slider-m-1.jpg 920w, data1/images/slider-s-1.jpg 450w" alt="MALLAS OLÍMPICAS" title="MALLAS OLÍMPICAS" id="wows1_0"/></li>
    <li><img src="data1/images/slider2.jpg" srcset="data1/images/slider2.jpg 1280w, data1/images/slider-m-2.jpg 920w, data1/images/slider-s-2.jpg 450w" alt="GAVIONES Y COLCHONETAS" title="GAVIONES Y COLCHONETAS" id="wows1_1"/></li>
    <li><img src="data1/images/slider3.jpg" srcset="data1/images/slider3.jpg 1280w, data1/images/slider-m-3.jpg 920w, data1/images/slider-s-3.jpg 450w" alt="PARANTES GALVANIZADOS" title="PARANTES GALVANIZADOS" id="wows1_2"/></li>
    <li><img src="data1/images/slider4.jpg" srcset="data1/images/slider4.jpg 1280w, data1/images/slider-m-4.jpg 920w, data1/images/slider-s-4.jpg 450w" alt="CONSTRUCCIÓN DE ESTRUCTURAS METÁLICAS" title="CONSTRUCCIÓN DE ESTRUCTURAS METÁLICAS" id="wows1_3"/></li>
    <li><img src="data1/images/slider6.jpg" srcset="data1/images/slider6.jpg 1280w, data1/images/slider-m-6.jpg 920w, data1/images/slider-s-6.jpg 450w" alt="PARQUES INFANTILES" title="PARQUES INFANTILES" id="wows1_5"/></li>
    </ul></div>
    <div class="ws_bullets"><div>
    <!-- <a href="#" title="MALLAS OLÍMPICAS"><img src="data1/tooltips/slider2.jpg" alt="MALLAS OLÍMPICAS"/>1</a>
    <a href="#" title="GAVIONES Y COLCHONETAS"><img src="data1/tooltips/slider4.jpg" alt="GAVIONES Y COLCHONETAS"/>2</a>
    <a href="#" title="PARANTES GALVANIZADOS"><img src="data1/tooltips/slider3.jpg" alt="PARANTES GALVANIZADOS"/>3</a>
    <a href="#" title="CONSTRUCCIÓN DE ESTRUCTURAS METÁLICAS"><img src="data1/tooltips/slider5.jpg" alt="CONSTRUCCIÓN DE ESTRUCTURAS METÁLICAS"/>4</a> -->
    </div></div>
    <span class="wsl"><a href="http://wowslider.com">Slider Web</a> by WOWSlider.com v4.7</span>
	<div class="ws_shadow"></div>
	</div>
	<!-- End WOWSlider.com BODY section -->
</div>
 <?php require('require/footer.php'); ?>
	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
</body>
</html>
