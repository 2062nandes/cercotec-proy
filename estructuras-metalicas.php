<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
 <?php require('require/menu.php'); ?>
<section class="container">
<div class="row">
  <div class="col s12">
  <h4>ESTRUCTURAS METÁLICAS</h4>
  <article>
   <div class="col s12 m12 l9 construccion">
     <div class="slider">
       <ul class="slides">
         <li>
           <img src="images/construccion/construccion2.jpg">
           <div class="caption center-align">
             <h3>FABRICACION DE SERCHAS Y TINGLADOS</h3>
             <h5 class="light grey-text text-lighten-3">Costaneras 100 x 50 x 2,5 m</h5>
           </div>
         </li>
         <li>
           <img src="images/construccion/construccion8.jpg">
           <div class="caption right-align">
             <h3>CONSTRUCCIÓN DE TINGLADOS</h3>
           </div>
         </li>
         <li>
           <img src="images/construccion/construccion9.jpg">
           <div class="caption center-align">
             <h3>CONSTRUCCIÓN DE ESTRUCTURAS METÁLICAS</h3>
           </div>
         </li>
       </ul>
     </div>
   </div>
   <div class="col s12 m12 l3"><br>
      <p>Recibimos trabajos como obra vendida. Nos encargamos de todo el equipamiento y la construcción en cualquier punto del departamento de La Paz (Ciudad y Provincias). Ofrecemos servicios especializados en todo tipo de equipamiento urbano, incluso, canchas de césped sintético.</p>
   </div>
    <div class="col s12">
      <ul class="col s12 m6 l6">
        <li><i class="fa fa-caret-right"></i>&nbsp; Fabricación de serchas y tinglados
            <br>&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i>&nbsp; Costaneras 100 x 50 x 2,5 m</li>
        <li><i class="fa fa-caret-right"></i>&nbsp; Fabricación de juegos para parques
            <br>&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i>&nbsp; Sube bajas, resbalines, etc</li>
        <li><i class="fa fa-caret-right"></i>&nbsp; Enmallado de parques infantiles
            <br>&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i>&nbsp; Refugios, bordillos, cercas altas, bajas</li>
        <li><i class="fa fa-caret-right"></i>&nbsp; Enmallado de campos deportivos</li>
      </ul>
      <ul class="col s12 m6 l6">
        <li><i class="fa fa-caret-right"></i>&nbsp; Instalación de mallas y gaviones</li>
        <li><i class="fa fa-caret-right"></i>&nbsp; Soldadura al arco y oxígeno</li>
        <li><i class="fa fa-caret-right"></i>&nbsp; Fabricación de portones (puertas de garaje)
            <br>&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i>&nbsp; Cañería de 2”. 3 x 2 m. Dos batientes</li>
        <li><i class="fa fa-caret-right"></i>&nbsp; Fabricación de puertas peatonales
            <br>&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i>&nbsp; Cañería de 2”. 1 x 2 m. Una batiente</li>
      </ul>
    </div>
  </article>
  </div>
</div>
</section>
 <?php require('require/footer.php'); ?>
</body>
</html>
