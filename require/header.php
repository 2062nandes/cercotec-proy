<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
  <link rel="icon" href="/favicon.png" type="image/png">
  <meta name="designer" content="Fernando Javier Averanga Aruquipa"/>
  <meta name="description" content="Mallas olímpicas. Gaviones y colchonetas. Parantes, postes y luminarias. Estructuras metálicas, serchas y tinglados. Construcción de enmallados de canchas deportivas, Cercos de seguridad, portones"/>
  <meta name="author" content=""/>
  <meta name="keywords" content="cercotec, mallas, olímpicas, bolivia"/>
  <title>Mallas olímpicas CERCOTEC – Gaviones y colchonetas</title> <meta name="description" content="Mudanzas local, nacional e internacional; embalajes, trámites aduaneros de exportación e importación; transporte de carga aérea, terrestre y marítima; seguros de carga, servicio de Courier."/>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="vendor/materialize/materialize.min.css"  media="screen,projection"/>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- Start WOWSlider.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
</head>
