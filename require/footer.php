<footer>
 <div class="container">
   <div class="row" style="margin-bottom:0;">
     <div class="col s12 m12 l9">
       <div class="fot col s12" style="padding: 0;">
         <h3>CERCOTEC BOLIVIA</h3>
        <div class="col s12 m12 l7">
          <p><b>Of. central:</b> La Paz - El Alto, Villa Santiago I, Av. 6 de Marzo<br>esq. Tiahuanacu N° 220 <a href="contactos.php"><i class="fa fa-map-marker"></i> VER MAPA</a><br>
          <b>Sucursal:</b> La Paz - El Alto, Av. 6 de marzo Nº 401<br>(a media cuadra del cruce a Viacha)</p>
          <p>
          <b><i class="fa fa-whatsapp fa-2x"></i> WhatsApp:</b> 77272819 | 
          <b><i class="fa fa-phone"></i> Cel.:</b> 76283379
          </p>
        </div>
        <div class="col s12 m12 l5">
          <p> 
          <b>Of. Tarija:</b> Av. Circunvalación #510<br>entre Av. Mejillones y Gral. Trigo.
          </p>
          <p>
          <b><i class="fa fa-phone"></i> Cels.:</b> 76150770 <b>/</b> 77272819
          </p>
        </div>
     </div>
    </div>
     <div class="col s12 m12 l3">
       <div class="fot col s12" style="padding: 0;">
       <h3>CONTÁCTENOS</h3>
       <div>
        <p class="center-align">
          <i class="fa fa-envelope"></i> info@mallascercotec.com<br>
          <i class="fa fa-envelope"></i> admin@mallascercotec.com
        </p>
         <ul class="social_icons center">
           <li><a target="_black" href="https://www.facebook.com/Cercotec-Mallas-Ol%C3%ADmpicas-769801063179904/"><i class="waves-effect waves-light fa fa-facebook"></i></a></li>
           <li><a target="_black" href="https://twitter.com/guiaGNB/status/788738698091323392"><i class="waves-effect waves-light fa fa-twitter"></i></a></li>
           <li><a target="_black" href="https://www.youtube.com/watch?v=wexb7SQp7zo"><i class=" waves-effect waves-light fa fa-youtube"></i></a></li>
        </ul>
       </div>
       </div>
     </div>
   </div>
    <div class="row creditos">
      <div class="col s12 m8 l7">
        <p class="center-align">Todos los Derechos Reservados &REG; <b>Mallas Olímpicas Cercotec Bolivia.</b> &COPY; <?=date("Y");?>.</p>
      </div>
      <div class="col s12 m4 l5">
        <p class="center-align ah"><a href="//ahpublic.com" target="_blank" id="ahpublicidad">Diseño y Programación Ah! Publicidad</a></p>
      </div>
    </div>
</footer>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.lazyload.js"></script>
  <script type="text/javascript" src="js/headroom.min.js"></script>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="vendor/materialize/materialize.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
