<header id="header" class="header fixed">
  <nav class="menu-principal hide-on-med-and-down">
    <div class="container">
    <center class="row" style="margin-bottom:0">
    <ul>
        <li class="col s12 m6 l2"><a class="waves-effect waves-yellow btn-mallas wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.95s" href="mallas-olimpicas.php">MALLAS<br>&nbsp;&nbsp;&nbsp;OLÍMPICAS</a></li>
        <li class="col s12 m6 l2"><a class="waves-effect waves-yellow btn-mallas wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s" href="gaviones-y-colchonetas.php">GAVIONES Y &nbsp;<br>&nbsp;&nbsp;&nbsp;COLCHONETAS</a></li>
        <li class="col s12 m6 l2"><a class="waves-effect waves-yellow btn-mallas wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.65s" href="postes-y-parantes-galvanizados.php">POSTES Y &nbsp;<br>&nbsp;&nbsp;&nbsp;PARANTES</a></li>
        <li class="col s12 m6 l2"><a class="waves-effect waves-yellow btn-mallas wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s" href="alambre-de-puas-y-cuchillas.php">ALAMBRE DE &nbsp;<br>&nbsp;&nbsp;&nbsp;PÚAS, CUCHILLAS</a></li>
        <li class="col s12 m6 l2"><a class="waves-effect waves-yellow btn-mallas wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.35s" href="estructuras-metalicas.php">ESTRUCTURAS &nbsp;<br>&nbsp;&nbsp;&nbsp;METÁLICAS</a></li>
        <li class="col s12 m6 l2"><a class="waves-effect waves-yellow btn-mallas wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.2s" href="canchas-y-parques-infantiles.php">CANCHAS Y &nbsp;<br>&nbsp;&nbsp;&nbsp;PARQUES</a></li>
    </ul>
    </center>
    </div>
  </nav>
  <nav class="menu-inicio">
    <div class="container">
      <a href="./" class="brand-logo right" >
  <h1 class="cercotec">
      <span class="c wow fadeIn" data-wow-duration="1s" data-wow-delay="3.4s">C</span><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="3s"><span>E</span></div><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="2.7s"><span>R</span></div><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="2.4s"><span>C</span></div><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="2.1s"><span>O</span></div><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="1.8s"><span>T</span></div><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="1.5s"><span>E</span></div><div class="wow rotateInDownRight" data-wow-duration="1s" data-wow-delay="1.2s"><span>C</span></div></h1>
        <h1 class="slogan1 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="4.2s">MALLAS OLIMPICAS</h1>
        <h1 class="slogan2 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="4.2s">GAVIONES Y COLCHONETAS</h1>
        <span class="wow fadeIn" data-wow-duration="1s" data-wow-delay="3.9s"><?php require('require/malla.php'); ?></span>
      </a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="2s"><a class="btn-inicio" href="./"><center><i class="waves-effect fa fa-home wow rotateIn" data-wow-duration="1s" data-wow-delay="2.1s"></i></center><center>Inicio</center></a></li>
        <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.7s"><a class="btn-inicio" href="nosotros.php"><center><i class="waves-effect fa fa-users wow rotateIn" data-wow-duration="1s" data-wow-delay="1.8s"></i></center><center>Nosotros</center></a></li>
        <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.4s"><a class="btn-inicio" href="contactos.php"><center><i class="waves-effect fa fa-envelope wow rotateIn" data-wow-duration="1s" data-wow-delay="1.5s"></i></center><center>Contactos</center></a></li>
      </ul>
    </div>
  </nav>
  <nav class="menu-principal">
    <div class="container">
    <center class="row" style="margin-bottom:0">
    <ul>
        <li class="col s12"><a data-activates="mobile-demo" class="waves-effect waves-yellow btn-mallas wow fadeInDown hide-on-large-only button-collapse" data-wow-duration="1s" data-wow-delay="0" href="#"><i class="fa fa-bars"></i> MENÚ</a></li>
    </ul>
    </center>
    </div>
  </nav>
</header>
<header>
  <nav class="menu-principal">
    <div class="container">
        <ul class="side-nav" id="mobile-demo">
          <center class="row">
          <a href="./" class="left"><img class="responsive-img" src="images/cercotec.png" alt="cercotec bolivia - mallas olímpicas"></a>
          <ul id="nav-mobile">
            <li><a class="btn-inicio" href="./"><center><i class="waves-effect fa fa-home"></i></center><center>Inicio</center></a></li>
            <li><a class="btn-inicio" href="nosotros.php"><center><i class="waves-effect fa fa-users"></i></center><center>Nosotros</center></a></li>
            <li><a class="btn-inicio" href="contactos.php"><center><i class="waves-effect fa fa-envelope"></i></center><center>Contactos</center></a></li>
          </ul>
          <li class="col s12"><a class="waves-effect waves-yellow btn-mallas" href="mallas-olimpicas.php">MALLAS<br>OLÍMPICAS</a></li>
          <li class="col s12"><a class="waves-effect waves-yellow btn-mallas" href="gaviones-y-colchonetas.php">GAVIONES Y<br>COLCHONETAS</a></li>
          <li class="col s12"><a class="waves-effect waves-yellow btn-mallas" href="postes-y-parantes-galvanizados.php">POSTES Y &nbsp;<br>&nbsp;&nbsp;&nbsp;PARANTES</a></li>
          <li class="col s12"><a class="waves-effect waves-yellow btn-mallas" href="alambre-de-puas-y-cuchillas.php">ALAMBRE DE &nbsp;<br>&nbsp;&nbsp;&nbsp;PÚAS, CUCHILLAS</a></li>
          <li class="col s12"><a class="waves-effect waves-yellow btn-mallas" href="estructuras-metalicas.php">ESTRUCTURAS &nbsp;<br>&nbsp;&nbsp;&nbsp;METÁLICAS</a></li>
          <li class="col s12"><a class="waves-effect waves-yellow btn-mallas" href="canchas-y-parques-infantiles.php">CANCHAS Y &nbsp;<br>&nbsp;&nbsp;&nbsp;PARQUES</a></li>
          </center>
        </ul>
    </div>
  </nav>
</header>
