jQuery(function($) {'use strict',
	//Initiat WOW JS
	new WOW().init();
	$(document).ready($(function() {
		    $("img").lazyload({
					threshold : 150,
    			effect : "fadeIn"
				});
				$('.slider').slider({interval: 3500});
				$('.materialboxed').materialbox();
				$(".button-collapse").sideNav();
				//headroom
				var header = document.querySelector("#header");
				new Headroom(header, {
					tolerance: {
						down : 1,
						up : 1
					},
					offset : 95,
					classes: {
						initial: "slide",
						pinned: "slide--reset",
						unpinned: "slide--up",
						top: "slide--top",
						notTop: "slide--notop"
					}
				}).init();
  }));
	/*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

	var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("/mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });

	/*Cotizacion gaviones y colchonetas*/
	$(function(){
		$('input, textarea').each(function() {
			$(this).on('focus', function() {
				$(this).parent('.input').addClass('active');
		 });
		if($(this).val() != '') $(this).parent('.input').addClass('active');
		});
	});

	var message = $('#statusMessage');
	$('.deletebtn').click(function() {
		message.removeClass("animMessage");
		$("#theForm2 input").css("box-shadow", "none");
		$("#theForm2 textarea").css("box-shadow", "none");
		$("#theForm2 select").css("box-shadow", "none");
		$(".input").removeClass("active");
	});
	$('.submitbtn2').click(function() {
		message.removeClass("animMessage");
		$("#theForm2 input").css("box-shadow", "");
		$("#theForm2 textarea").css("box-shadow", "");
		$("#theForm2 select").css("box-shadow", "");
		if($("#theForm2")[0].checkValidity()){
			$.post("../cotizacion.php", $("#theForm2").serialize(), function(response) {
				if (response == "enviado"){
					$("#theForm2" )[0].reset();
					$(".input").removeClass("active");
					message.html("Su solicitud fue envíada correctamente,<br>le responderemos en breve");
				}
				else{
					message.html("Su solicitud no pudo ser envíado");
				}
				message.addClass("animMessage");
				$("#theForm2 input").css("box-shadow", "none");
				$("#theForm2 textarea").css("box-shadow", "none");
				$("#theForm2 select").css("box-shadow", "none");
			});
			return false;
		}
	});

});
